#!/usr/bin/env zsh
zsh update-arch.zsh
zsh update-rpm.zsh

# the debian script will perform validation and extraction
zsh update-ubuntu.zsh

zsh update-debian.zsh

zsh harvest-docker.zsh docker/ubuntu:focal
zsh harvest-docker.zsh docker/ubuntu:bionic
zsh harvest-docker.zsh docker/ubuntu:trusty

zsh update-hashes.zsh

