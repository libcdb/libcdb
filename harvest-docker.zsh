#!/usr/bin/env zsh
set -e

if [ -z "$1" ]; then
    >&2 cat <<EOF
usage: ${0:t} directory
EOF
    exit 1
fi

# Where is the Dockerfile
docker=$1

# Root of libcdb git repository
root="$(git rev-parse --show-toplevel)"

# Directory that contains sha256 hashes of known files
hashes="$root/hashes/sha256"

# Today's date
date=$(date +"%Y-%m-%d")

# Destination directory for new libc we discovered today
dest="$root/$docker/$date"
mkdir -p $dest

# Docker tag to extract files from
tag="${docker:a:t}".libc
context="$docker/context"
Dockerfile="$context/Dockerfile"

# Workaround for running from macOS
SED=sed
(( $+commands[gsed] )) && SED=gsed

# Update the base image
base_image=$(grep -E "^FROM" $Dockerfile | awk '{print $2}')
docker pull "$base_image"

# Make the docker image, and force it to re-update
$SED -i -E "s|apt-get update.*|apt-get update # ${date}|" $Dockerfile
docker build -t ${tag} $context

# Copy a file out of the docker image
function copy() {
    docker cp $(docker create --rm $tag):$1 $2
}

# Remove all files from any previous runs
files_and_hashes=$(docker run --rm "$tag" sh -c "find /lib /usr -iname 'libc.so.6' | xargs realpath | xargs sha256sum")
declare -A files=( $(echo $files_and_hashes[*]) )

for hash file in "${(@kv)files}"; do
    # Replace slashes in full pathname with underscores, and strip the first one
    filename=$(echo $file | sed -e 's|/|_|g' | sed 's|_||')

    # Full path to where the file should go
    target="$dest/$filename"

    echo "Docker says: $target"

    if [ -e "$hashes/$hash" ] && [ -e "$(realpath $hashes/$hash)" ]; then
        echo Already exists: "$hashes/$hash"
        continue
    fi

    # Grab the file if we don't already have it
    if ! [ -f "$target" ]; then
        copy $file $target
    fi
done

git add "$dest"
git commit -m "Harvest files from $docker"

cd "$root"
$root/update-hashes.zsh "$dest"