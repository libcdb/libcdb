#!/usr/bin/env zsh

# Find the current date
date=date

if (( $+commands[gdate] )); then
    date=gdate
fi

date=$($date --iso-8601)

# List of docker images to pull from daily
PROJECTS=(
# Arch
    archlinux:latest
    archlinux:base

# Centos
    centos:8
    centos:7
    centos:6

# Debian
    debian:bullseye
    debian:buster
    debian:jessie
    debian:sid

# Fedora
    fedora:34
    fedora:33
    fedora:32
    fedora:31

# Ubuntu
    ubuntu:trusty
    ubuntu:xenial
    ubuntu:bionic
    ubuntu:focal
    ubuntu:groovy
    ubuntu:hirsute
)

UPDATED=false

git pull

# For each docker image, pull out the libc
for project in "${PROJECTS[@]}"; do
    LIBC=$(docker run --rm -t -e LD_TRACE_LOADED_OBJECTS=1 $project | grep -E -o '/.*libc.so\S+')
    
    docker run --rm $project cat "$LIBC" >! libc.so

    sha256sum=$(sha256sum libc.so | awk '{print $1}')

    if [ -e hashes/sha256/$sha256sum ]; then
        echo "=== Skipping existing $project libc.so ==="
    else
        mkdir -p docker/$project/$date/
        mv libc.so docker/$project/$date/
        git add docker/$project/$date/
        git commit -m "$project $date"
        UPDATED=true
    fi

    rm -f libc.so
done

if $UPDATED; then
    docker image prune --force
    ./update-hashes.zsh
    git push
fi