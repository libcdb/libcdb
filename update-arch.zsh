#!/usr/bin/env zsh

tar=tar
if (( $+commands[gtar] )); then
    tar=gtar
fi

function is_marked {
    if file "$1" | grep Zstandard > /dev/null;
    then
        false
        return
    fi

    # Ensure the commit actually exists
    commit="$(head -c40 $1)"
    if [ "$(git cat-file -t $commit 2>/dev/null)" = "commit" ];
    then
        true
        return
    fi

    false
    return
}

function mark {
    rm -f $1
    echo $2 > $1
}


function check {
    echo Implement me... "$@"
}

function download() {
    URLS=(
        "https://archlinux.pkgs.org/rolling/archlinux-core-x86_64/"
        "https://archive.archlinux.org/packages/g/glibc/"
        "http://mirror.archlinuxarm.org/armv7h/core/"
        "http://mirror.archlinuxarm.org/arm/core"
        "http://mirror.archlinuxarm.org/aarch64/core/"
    )


    for URL in $URLS;
    do
        wget \
         --follow-ftp \
         --no-parent \
         --execute robots=off \
         --no-proxy \
         --level=1 \
         --no-parent \
         --recursive \
         --no-clobber \
         --no-directories \
         --accept "glibc*.tar.zst" \
         --accept "glibc*.tar.xz" \
         --verbose \
         --directory-prefix $1 \
         $URL
    done
}



function extract {
find $1 -iname '*.tar.zst' -o -iname '*.tar.xz' -type f | while read zst
do
    echo Extracting $zst
    dir="libc/${zst:t:r}"

    echo "Checking $zst"
    if is_marked "$zst" ;
    then
        echo "...skipping $zst"
        continue
    fi

    [[ -d $dir ]] || mkdir -p $dir

    echo "Extracting $zst"
    $tar \
        --directory "$dir" \
        --wildcards \
        --wildcards-match-slash \
        --extract \
        -m \
        --file "$zst" \
        '*libc.so.*' \
        '*libc-*.so*' \
        '*libc.a' \
        '*libc.so*'

    echo "Committing $zst"
    git add libc || continue
    git commit -m "$zst"

    mark $zst $(git rev-parse HEAD)
    git add $zst
    git commit -m "update .tar.zst"
done
}

case "$1" in
    check)
        check    archfiles
        ;;
    download)
        download archfiles
        ;;
    extract)
        extract  archfiles
        ;;
    *)
        check    archfiles
        download archfiles
        extract  archfiles
        ;;
esac
