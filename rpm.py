#!/usr/bin/env python3
import argparse
import requests
import multiprocessing
import os
import sys
import time
import urllib

from pprint import pprint
from html.parser import HTMLParser
from urllib.parse import urljoin

URLs = [
# "http://rpmfind.net/linux/rpm2html/search.php?query=libc.so.6",
]

FedoraArchive = [
"https://dl.fedoraproject.org/pub/archive/fedora/linux/",
"https://archives.fedoraproject.org/pub/fedora/linux/"
]

FedoraIndices = [
    "updates/%(release)s/%(arch)s/",
    "releases/%(release)s/Everything/%(arch)s/os/Packages/%(prefix)s",
    "releases/%(release)s/Everything/%(arch)s/os/Packages/"
]

FedoraArches = ['armhfp',
                'i386',
                'x86_64',
                'ppc',
                'ppc64']
FedoraArches.extend([i + '.newkey' for i in FedoraArches])

# glibc, dietlibc
PackagePrefixes = ["g","d"]

for archive in FedoraArchive:
    for index in FedoraIndices:
        for release in range(7,31):
            for arch in FedoraArches:
                for prefix in PackagePrefixes:
                    url = archive + index
                    url = url % locals()
                    print(url)
                    URLs.append(url)

URL_BASE = 'https://archives.fedoraproject.org/pub/archive/fedora/linux/core/%(release)s/%(arch)s/os/Fedora/RPMS/'

URLs = []

for release in range(1, 7):
    for arch in FedoraArches:
        URLs.append(URL_BASE % locals())

pprint(URLs)

# print("=== Fetching ===")
# pprint(URLs)

blacklist = [
'-bin',
'-doc',
'-dbg',
'-debug',
'-prof',
'-source',
'-src',
'-xen',
'udeb',
'linux-libc-dev',
'-pic',
'-utils',
'-header',
'-common',
'-devel',
'msp430',
'avr',
'delta',
'kernheader',
'langpack'
]

whitelist = [
'/glibc',
'dietlibc',
'-libc-',
'-glibc-',
]


Q_rpm = []
Q_dir = []


URLs.reverse()

for URL in URLs:
    Q_dir.append(URL)

def curl(url):
    try:
        r = requests.get(url, timeout=5)
    except Exception:
        print('Error: %s' % (url))
        return None

    print('%s: %s' % (r.status_code, url))
    if r.ok: return r.content

class WgetMirror(HTMLParser):
    def __init__(self, url):
        HTMLParser.__init__(self)
        self.url = url

    def handle_starttag(self, tag, attrs):
        if tag != 'a': return

        attrs = dict(attrs)
        href = attrs.get('href', None)
        if not href:
            return

        if not href.endswith('rpm'):
            return

        href = href.replace('ftp://', 'http://')

        if '://' not in href:
            href = urljoin(self.url, href)

        if any(B in href for B in blacklist):
            # print("Skipping [B]", href)
            return

        if not any(W in href for W in whitelist):
            # print("Skipping [!W]", href)
            return

        basename = os.path.basename(href)

        if not os.path.exists(basename):
            print("Keeping", href)
            Q_rpm.append(href)

def rpm_worker(L):
    for url in iter(L):
        print('rpm', url)
        path = os.path.basename(url)

        if any(B in url for B in blacklist):
            # print "Black: ", path
            continue

        if not any(W in url for W in whitelist):
            # print "!White:", path
            continue

        if os.path.exists(path):
            print("Skip: ", path)
            continue

        with open(path, 'wb+') as f:
            data = curl(url)
            if not data:
                continue
            f.write(data)

def dir_worker(L):
    for url in iter(L):
        print('dir', url)
        html = curl(url)
        if html:
            html = html.decode()
            WgetMirror(url).feed(html)

def main():
    os.chdir('rpmfiles')

    dir_worker(Q_dir)
    rpm_worker(Q_rpm)

    # for function, queue in queues.items():
    #     if threads == 1:
    #         print(function)
    #         function(queue)
    #     else:
    #         p = multiprocessing.Process(target=function, args=(queue,))
    #         for w in range(threads):
    #             p = multiprocessing.Process(target=function, args=(queue,))
    #             p.start()
    #             workers.append(p)


p = argparse.ArgumentParser()

if __name__ == '__main__':
    main()